package main

import (
	"github.com/gorilla/mux"
	"reflect"
	"testing"
)

func TestHomeHandler(t *testing.T) {
	type args struct {
		r *mux.Router
	}
	tests := []struct {
		name string
		args args
		want *mux.Route
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := HomeHandler(tt.args.r); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("HomeHandler() = %v, want %v", got, tt.want)
			}
		})
	}
}
