package service

import (
	"context"
	"errors"
	"fmt"
	"go-kit-grpc-example/internal/core/transport"
	"google.golang.org/grpc"
	"os"
	"time"

	"github.com/go-kit/kit/log"
)

type service struct {
	logger *log.Logger
}

// Service interface describes a service that adds numbers
type Service interface {
	Add(ctx context.Context, numA, numB float32) (float32, error)
}

// NewService returns a Service with all of the expected dependencies
func NewService(logger *log.Logger) Service {
	return &service{
		logger: logger,
	}
}

// Add func implements Service interface
func (s service) Add(ctx context.Context, numA, numB float32) (float32, error) {
	conn, err := grpc.Dial("localhost:50052", grpc.WithInsecure(), grpc.WithTimeout(time.Second))
	if err != nil {
		fmt.Fprintf(os.Stderr, "error: %v", err)
		os.Exit(1)
	}
	defer conn.Close()
	svc := transport.NewGRPCClient(conn,s.logger)
	res,err := svc.Add(ctx,numA,numB)

	return numA + numB, nil
}

var (
	ErrNotFound        = errors.New("not found")
)