package main

import (
	"flag"
	"fmt"
	"github.com/google/uuid"
	consulapi "github.com/hashicorp/consul/api"
	"go-kit-grpc-example/internal/consul"
	"google.golang.org/grpc/health/grpc_health_v1"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	consulsd "github.com/go-kit/kit/sd/consul"
	"go-kit-grpc-example/internal/gateway/endpoints"
	"go-kit-grpc-example/internal/gateway/service"
	"go-kit-grpc-example/internal/gateway/transport"
	"go-kit-grpc-example/pkg/pb"
	"google.golang.org/grpc"
)

const (
	application = "core"
)

func main() {
	var (
		consulAddr   = flag.String("consul.addr", "dcr:8500", "Consul agent address")
		grpcPort = flag.String("grpc.port", ":50052", "GRPC server port")
	)
	flag.Parse()

	var logger log.Logger
	logger = log.NewJSONLogger(os.Stdout)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	logger = log.With(logger, "caller", log.DefaultCaller)

	// Service discovery domain. In this example we use Consul.
	var client consulsd.Client
	{
		consulConfig := consulapi.DefaultConfig()
		if len(*consulAddr) > 0 {
			consulConfig.Address = *consulAddr
		}
		consulClient, err := consulapi.NewClient(consulConfig)
		if err != nil {
			logger.Log("err", err)
			os.Exit(1)
		}
		client = consulsd.NewClient(consulClient)
	}
	//ip := utils.LocalIP()
	asr := consulapi.AgentServiceRegistration{
		ID:      uuid.New().String(),
		Name:    application,
		Address: "192.168.0.253",
		Port:    50051,
		Tags:    []string{},
		Check:   &consulapi.AgentServiceCheck{
			Interval:                       (time.Duration(10) * time.Second).String(),
			GRPC:                           fmt.Sprintf("%v%v/%v", "192.168.0.253", *grpcPort, application),
			DeregisterCriticalServiceAfter: (time.Duration(1) * time.Minute).String(),
		},
	}
	registar := consulsd.NewRegistrar(client,&asr,logger)
	registar.Register()
	defer registar.Deregister()

	addservice := service.NewService(logger)
	addendpoint := endpoints.MakeEndpoints(addservice)
	grpcServer := transport.NewGRPCServer(addendpoint, logger)

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGALRM)
		errs <- fmt.Errorf("%s", <-c)
	}()

	grpcListener, err := net.Listen("tcp", *grpcPort)
	if err != nil {
		logger.Log("during", "Listen", "err", err)
		os.Exit(1)
	}

	go func() {
		baseServer := grpc.NewServer()
		pb.RegisterMathServiceServer(baseServer, grpcServer)
		level.Info(logger).Log("msg", "Server started successfully")
		errs <- baseServer.Serve(grpcListener)
		grpc_health_v1.RegisterHealthServer(baseServer, &consul.HealthImpl{Status: grpc_health_v1.HealthCheckResponse_SERVING})
	}()

	level.Error(logger).Log("exit", <-errs)
}